# filler
The goal of project to write your own bot that will defeat all other bots.

### skills ###
- basic algorithmic skills (creation of my own filling algorithm)
- working with I/O streams

### usage ###
- cd resources

	./filler_vm -f [maps/map] -p1 [./knovytsk.filler] -p2 [players/player2]

	./filler_vm -f [maps/map] -p1 [players/player2] -p2 [./knovytsk.filler]

- visualization

	./filler_vm -f [maps/map] -p1 [./knovytsk.filler] -p2 [players/player2]  | ../visual/visualizer.knovytsk